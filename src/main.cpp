#include "includes.h"

#include "shader.h"
#include "gui.h"
#include "random.h"

int main()
{
    ////////////////////////////////////////
    // BOILERPLATE
    ////////////////////////////////////////

    Uint32 seed = 210551297;

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        printf("Failed SDL_Init: %s\n", SDL_GetError());
        return 1;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

    SDL_Window* window;

    window = SDL_CreateWindow("mini_toy",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED,
                              SCREEN_WIDTH,
                              SCREEN_HEIGHT,
                              SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

    if (window == NULL)
    {
        printf("Failed SDL_CreateWindow()\n");
        return 1;
    }

    SDL_GLContext context = SDL_GL_CreateContext(window);

    if (context == NULL)
    {
        printf("Failed SDL_GL_CreateContext()\n");
        return 1;
    }

    glewExperimental = GL_TRUE;
    GLenum glewError = glewInit();

    if (glewError != GLEW_OK)
    {
        printf("Failed glewInit()\n");
        return 1;
    }

    ImGui_ImplGlfwGL3_Init();

    SDL_GL_SetSwapInterval(1);

    float fullscreenQuad[] =
    {
        // position   uv
        -1.0f, -1.0f, 0.0f, 0.0f,
        -1.0f, +1.0f, 0.0f, 1.0f,
        +1.0f, -1.0f, 1.0f, 0.0f,
        +1.0f, +1.0f, 1.0f, 1.0f,
    };

    int fullscreenIndices[] =
    {
        0, 1, 3,
        0, 3, 2
    };

    const char* vs =
        "#version 330\n"
        "in vec2 inPosition;\n"
        "in vec2 inUv;\n"
        "out vec2 uv;\n"
        "void main()\n"
        "{\n"
        "\tuv = inUv;"
        "\tgl_Position = vec4(inPosition, 0.0, 1.0);\n"
        "}";

    const int BUF_SZ = 8192;
    char fs[BUF_SZ] = { 0 };

    const char* fsTemplate =
        "#version 330\n"
        "\n"
        "in vec2 uv;\n"
        "\n"
        "uniform float time;\n"
        "uniform vec2 resolution;\n"
        "\n"
        "out vec4 fragColor;\n"
        "\n"
        "void main()\n"
        "{\n"
        "\tfloat s = 0.5 + 0.5 * sin(time);\n"
        "\tfragColor = vec4(uv.xy, s, 1.0);\n"
        "}";

    strcat(fs, fsTemplate);

    GLuint shaderProgram;
    if (!createShader(vs, fs, &shaderProgram))
    {
        return 1;
    }

    glUseProgram(shaderProgram);

    GLuint fullscreenVao;
    glGenVertexArrays(1, &fullscreenVao);
    glBindVertexArray(fullscreenVao);

    GLuint fullscreenVbo;
    glGenBuffers(1, &fullscreenVbo);
    glBindBuffer(GL_ARRAY_BUFFER, fullscreenVbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(fullscreenQuad), fullscreenQuad, GL_STATIC_DRAW);

    GLuint fullscreenIbo;
    glGenBuffers(1, &fullscreenIbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, fullscreenIbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(fullscreenIndices), fullscreenIndices, GL_STATIC_DRAW);

    GLint posAttrib = glGetAttribLocation(shaderProgram, "inPosition");
    GLint uvAttrib = glGetAttribLocation(shaderProgram, "inUv");
    int uvOffset = 2 * sizeof(float);
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
    glEnableVertexAttribArray(uvAttrib);
    glVertexAttribPointer(uvAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)uvOffset);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    ////////////////////////////////////////
    // CONFIGURATION
    ////////////////////////////////////////

    bool running = true;

    Uint32 ticks = SDL_GetTicks();
    Uint32 totalTicks = ticks;

    struct ShaderUniforms
    {
        int time;
        int resolution;
    } uniforms;

    memset(&uniforms, -1, sizeof(uniforms));
    uniforms.time = glGetUniformLocation(shaderProgram, "time");
    uniforms.resolution = glGetUniformLocation(shaderProgram, "resolution");
    glUniform1f(uniforms.time, 0.0f);

    while (running)
    {
        ////////////////////////////////////////
        // TIMING
        ////////////////////////////////////////

        Uint32 elapsedTicks = SDL_GetTicks() - ticks;
        ticks = SDL_GetTicks();
        totalTicks += elapsedTicks;

        float deltaTime = (float)elapsedTicks / 1000.0f;
        float totalTime = (float)totalTicks / 1000.0f;

        ////////////////////////////////////////
        // EVENTS
        ////////////////////////////////////////

        SDL_Event event;

        while (SDL_PollEvent(&event))
        {
            ImGui_ImplSdl_ProcessEvent(&event);

            if (event.type == SDL_QUIT)
            {
                running = false;
            }
        }

        ////////////////////////////////////////
        // GUI
        ////////////////////////////////////////

        ImGui_ImplGlfwGL3_NewFrame(window);

        ImGui::Begin("editor");

        ImGui::InputTextMultiline("", fs, BUF_SZ, ImVec2(600.0f, 600.0f),
                                  ImGuiInputTextFlags_AllowTabInput);

        if (ImGui::Button("run"))
        {
            GLuint newProgram;

            if (createShader(vs, fs, &newProgram))
            {
                glUseProgram(0);
                glDeleteProgram(shaderProgram);
                shaderProgram = newProgram;

                glUseProgram(shaderProgram);
                uniforms.time = glGetUniformLocation(shaderProgram, "time");
                uniforms.resolution = glGetUniformLocation(shaderProgram, "resolution");

                glUniform2f(uniforms.resolution, SCREEN_WIDTH, SCREEN_HEIGHT);

                printf("Compilation success!\n");
            }
        }

        ImGui::SameLine();

        if (ImGui::Button("save"))
        {
            FILE* f = fopen("shader_output.txt", "w");
            fwrite(fs, strlen(fs), 1, f);
            fclose(f);
        }

        ImGui::End();

        ImVec4 clear_color = ImColor(114, 144, 154);
        glViewport(0, 0, (int)ImGui::GetIO().DisplaySize.x, (int)ImGui::GetIO().DisplaySize.y);
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(shaderProgram);
        glBindVertexArray(fullscreenVao);

        if (uniforms.time != -1)
        {
            glUniform1f(uniforms.time, totalTime);
        }

        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        glBindVertexArray(0);
        glUseProgram(0);

        ImGui::Render();

        SDL_GL_SwapWindow(window);
    }

    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);

    return 0;
}