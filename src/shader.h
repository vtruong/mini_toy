#pragma once

int createShader(const char* vertexShaderSource, const char* fragmentShaderSource, GLuint* outShader)
{
    GLuint vertexShaderProgram;

    vertexShaderProgram = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShaderProgram, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShaderProgram);

    {
        GLint status;
        glGetShaderiv(vertexShaderProgram, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            char buffer[2048];
            memset(buffer, 0, sizeof(buffer));
            glGetShaderInfoLog(vertexShaderProgram, sizeof(buffer), NULL, buffer);
            printf("Vertex shader compilation error: %s\n", buffer);
            return 0;
        }
    }

    GLuint fragmentShaderProgram;

    fragmentShaderProgram = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShaderProgram, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShaderProgram);

    {
        GLint status;
        glGetShaderiv(fragmentShaderProgram, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE)
        {
            char buffer[2048];
            memset(buffer, 0, sizeof(buffer));
            glGetShaderInfoLog(fragmentShaderProgram, sizeof(buffer), NULL, buffer);
            printf("Fragment shader compilation error: %s\n", buffer);
            return 0;
        }
    }

    GLuint shaderProgram;

    shaderProgram = glCreateProgram();

    glAttachShader(shaderProgram, vertexShaderProgram);
    glAttachShader(shaderProgram, fragmentShaderProgram);

    glBindFragDataLocation(shaderProgram, 0, "outColor");

    glLinkProgram(shaderProgram);

    {
        GLint status;
        glGetProgramiv(shaderProgram, GL_LINK_STATUS, &status);

        if (status == GL_FALSE)
        {
            char buffer[2048];
            memset(buffer, 0, sizeof(buffer));
            glGetProgramInfoLog(shaderProgram, sizeof(buffer), NULL, buffer);
            printf("Program link error: %s\n", buffer);
            return 0;
        }
    }

    *outShader = shaderProgram;

    return 1;
}