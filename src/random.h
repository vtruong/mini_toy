#pragma once

#include "includes.h"

static real32 randf(u32* seed)
{
    *seed = (214013 * (*seed) + 2531011);
    return (real32)(((*seed) >> 16) & 0x7FFF) / 32768.0f;
}

static real32 randf(real32 min, real32 max, u32* seed)
{
    return min + randf(seed) * (max - min);
}

static vec3 randf3(u32* seed)
{
    real32 x = randf(seed);
    real32 y = randf(seed);
    real32 z = randf(seed);

    vec3 v;

    v.x = x;
    v.y = y;
    v.z = z;

    return v;
}

static vec3 randf3(real32 min, real32 max, u32* seed)
{
    return vec3(min) + randf3(seed) * (max - min);
}

static vec3 randf3(vec3 min, vec3 max, u32* seed)
{
    return min + randf3(seed) * (max - min);
}